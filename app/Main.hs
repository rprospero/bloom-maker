module Main where

import Control.Monad.IO.Class (liftIO, MonadIO)
import Control.Monad.Trans.Except
import Data.Bloom.MetaData
import Loader
import qualified Data.Vector as V
import Options.Applicative

data ExportOptions = RawBloom Int Int FilePath
                   | SteadyBloom Double FilePath

parserRawBloom :: Parser ExportOptions
parserRawBloom = RawBloom
                 <$> argument auto (metavar "BITS")
                 <*> argument auto (metavar "HASHES")
                 <*> strArgument (metavar "OUTFILE")
parserSteadyBloom :: Parser ExportOptions
parserSteadyBloom = SteadyBloom
                    <$> argument auto (metavar "RATE")
                    <*> strArgument (metavar "OUTFILE")

exportOptions :: Parser ExportOptions
exportOptions = subparser $
  command "raw"
  (parserRawBloom `withInfo` "Store bloom filter of a defined size.")
  <> command "steady"
  (parserSteadyBloom `withInfo` "Store a bloom filter with a fixed false positive rate")


exportData :: (MonadIO m) => ExportOptions -> V.Vector MetaData -> DataSet -> m ()
exportData (RawBloom bits hashes f) m d = liftIO . exportBloom f m . toBloom bits hashes $ d
exportData (SteadyBloom rate f) m d = exportData (RawBloom bits hashes f) m d
  where
    n = (V.length d)
    bits = ceiling $ fromIntegral n * (log rate - log (fromIntegral $ permutations m)) / log (1.0 / (2.0 ** log 2.0))
    hashes = round $ log (2.0 :: Double) * fromIntegral bits / fromIntegral n

data Command = Compress ImportOptions [Column] ExportOptions
             | Query ImportOptions BloomLoader

parserCompress :: Parser Command
parserCompress = Compress <$> readOptions
                 <*> argument auto (metavar "COLUMNS")
                 <*> exportOptions
parserQuery :: Parser Command
parserQuery = Query <$> readOptions
              <*> bloomOptions

commandPrompt :: Parser Command
commandPrompt = subparser $
  command "compress"
   (parserCompress `withInfo` "Compress a data set")
  <> command "query"
   (parserQuery `withInfo` "Match a bloom filter against a data source")

runCommand :: MonadIO m => Command -> ExceptT String m ()
runCommand (Compress importOptions cols exporter) = do
  result <- loadData importOptions
  let meta = generateMetaData (V.fromList cols) result
  exportData exporter meta result
runCommand (Query importOptions bloomLoader) = do
  result <- loadData importOptions
  (bf, m) <- loadBloom bloomLoader
  liftIO . print $ V.concatMap (maskedQuery bf m) result


main :: IO ()
main = do
  cmd <- execParser (commandPrompt `withInfo` "A general utility for accessing data through bloom filters.")
  result <- runExceptT $ runCommand cmd
  case result of
    Right _ -> do
      print "Process complete"
    Left e -> do
      print "Error parsing data file:\n"
      print e
