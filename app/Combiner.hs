import Control.Monad.IO.Class
import Control.Monad.Trans.Except
import Data.Bloom
import Data.Bloom.MetaData
import Loader
import Options.Applicative

data Combiner = Combiner BloomLoader BloomLoader String

parseCombiner :: Parser Combiner
parseCombiner = Combiner <$> bloomOptions
  <*> bloomOptions
  <*> strArgument (metavar "OUTPUT" <> help "Where to save the combined result")

main :: IO ()
main = do
  Combiner a b f <- execParser $ info (helper <*> parseCombiner) (fullDesc <> progDesc "Combine two Bloom filters into a third file")
  result <- runExceptT $ do
    (abf, am) <- loadBloom a
    (bbf, bm) <- loadBloom b
    if am /= bm
      then throwE "Bloom filters have different metadata"
      else ExceptT (return $ combine abf bbf) >>= liftIO . exportBloom f am
  case result of
    Right _ -> do
      print "Files Merged"
    Left e -> do
      print "Error!"
      print e
