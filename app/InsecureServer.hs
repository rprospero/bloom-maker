{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.IO.Class (liftIO)
import qualified Data.ByteString.Lazy as BSL
import Options.Applicative
import System.FilePath ((</>))
import Web.Scotty

data ServerOptions = ServerOptions {port :: Int,
                                    directory :: FilePath}

parser :: Parser ServerOptions
parser = ServerOptions
  <$> option auto
    (long "port" <> short 'p' <> value 80
      <> metavar "PORT"
      <> help "The port for the server.")
  <*> strOption
    (long "directory" <> short 'd' <> value "./"
      <> metavar "DIRECTORY"
      <> help "The directory with the files to serve and where the uploads should be saved.")

main :: IO ()
main = do
  opts <- execParser $ info (helper <*> parser) (fullDesc <> progDesc "Serve a directory over HTTP, allowing files to be both downloaded and uploaded (via HTTP PIT)")
  scotty (port opts) $ do
    get "/:word" $ do
      f <- param "word"
      file (directory opts </> f)
    put "/:word" $ do
      f <- param "word" :: ActionM String
      liftIO $ print (directory opts </> f)
      contents <- body
      liftIO . BSL.writeFile (directory opts </> f) $ contents
      liftIO . print $ contents
      return ()
