{-# LANGUAGE DeriveGeneric #-}
module Data.Bloom.MetaData (generateMetaData,Column(..), MetaData(..), exportBloom, permutations, permute,Record, exportRecord, maskedQuery, DataSet, toBloom) where

import Data.Aeson
import Data.Binary (encodeFile)
import qualified Data.ByteString.Lazy as BSL
import Data.ByteString.Lazy.UTF8 (fromString)
import Data.Bloom
import Data.List (nub,sort)
import Data.Maybe (isJust,fromJust,fromMaybe)
import qualified Data.Text as T
import qualified Data.Vector as V
import GHC.Generics
import Options.Applicative
import Safe (readMay)
import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen (oneof)

type Record = V.Vector T.Text
type RecordMask = V.Vector (Maybe T.Text)

data Column = Field T.Text |
              Ignore |
              Enum T.Text |
              Numbers T.Text
            deriving (Show, Read)

data MetaData = Entry T.Text |
                Option T.Text (V.Vector T.Text) |
                Range T.Text Int Int
              deriving (Show, Eq, Generic, Ord)

instance Arbitrary MetaData where
  arbitrary = oneof [Entry . T.pack <$> arbitrary,
                     Option <$> (T.pack <$> arbitrary)
                      <*> (V.map T.pack . V.fromList<$> arbitrary),
                     Range <$> (T.pack  <$> arbitrary)
                      <*> arbitrary
                      <*> arbitrary]

instance ToJSON MetaData where
  toJSON (Entry name) = object [T.pack "kind" .= T.pack "Entry",
                                T.pack "label" .= name]
  toJSON (Option name vs) = object [T.pack "kind" .= T.pack "Option",
                                    T.pack "label" .= name,
                                    T.pack "options" .= toJSON (V.toList vs)]
  toJSON (Range name low high) = object [T.pack "kind" .= T.pack "Range",
                                         T.pack "label" .= name,
                                         T.pack "low" .= toJSON low,
                                         T.pack "high" .= toJSON high]

instance FromJSON MetaData where
  parseJSON (Object v) = do
    kind <- v .: T.pack "kind"
    case kind of
      "Entry" ->  Entry . T.pack <$> v .: T.pack "label"
      "Option" -> Option <$> v .: T.pack "label"
                 <*> v .: T.pack "options"
      "Range" -> Range <$> v .: T.pack "label"
                       <*> v .: T.pack "low"
                       <*> v .: T.pack "high"
      _ -> fail "Not a valid MetaData kind"
  parseJSON _ = fail "Not a Metadata object"


generateMetaData' :: Column -> V.Vector T.Text -> Maybe MetaData
generateMetaData' (Field name) _ = Just $ Entry name
generateMetaData' (Ignore) _ = Nothing
generateMetaData' (Enum name) xs = Just . Option name . V.fromList . sort . nub . V.toList $ xs
generateMetaData' (Numbers name) xs = Range name <$> (V.minimum <$> vals) <*> (V.maximum <$> vals)
  where
    vals :: Maybe (V.Vector Int)
    vals = V.sequence $ V.map (readMay . T.unpack) xs

generateMetaData :: V.Vector Column -> DataSet -> V.Vector MetaData
generateMetaData cs = catMaybes . (V.map (uncurry generateMetaData') . V.zip cs) . vTrans

vTrans :: V.Vector (V.Vector y) -> V.Vector (V.Vector y)
vTrans x
  | V.null x = V.empty
  | V.null (V.head x) = vTrans $ V.tail x
  | otherwise = V.map V.head x `V.cons` vTrans (V.map V.tail x)

catMaybes :: (Applicative f, Foldable f, Monoid (f (Maybe x))) => f (Maybe x) -> f x
catMaybes = fmap fromJust . ffilter isJust

ffilter :: (Applicative f, Foldable f, Monoid (f a)) => (a -> Bool) -> f a -> f a
ffilter p = foldMap (\a -> if p a then pure a else mempty)

exportBloom :: FilePath -> V.Vector MetaData -> BloomFilter -> IO ()
exportBloom f m b = do
  encodeFile f b
  BSL.writeFile (f <> ".json") $ encode m

permutations' :: MetaData -> Int
permutations' (Entry _) = 1
permutations' (Option _ x) = length x
permutations' (Range _ a b) = b-a+1

permutations :: V.Vector MetaData -> Int
permutations = V.product . V.map permutations'

permute' :: MetaData -> Maybe (V.Vector T.Text)
permute' (Entry _) = Nothing
permute' (Option _ x) = Just x
permute' (Range _ a b) = Just . V.map (T.pack . show) . V.fromList $ [a..b]

permute :: V.Vector MetaData -> V.Vector RecordMask
permute = sequence . V.map (sequence . permute')

withMask :: Record -> V.Vector RecordMask -> V.Vector Record
withMask r = V.map (overlay r)
  where
    overlay :: Record -> RecordMask -> Record
    overlay = V.zipWith fromMaybe


exportRecord :: Record -> BSL.ByteString
exportRecord = fromString . T.unpack . T.intercalate (T.pack "\RS") . V.toList

maskedQuery :: BloomFilter -> V.Vector MetaData -> Record -> V.Vector Record
maskedQuery bf mask r = V.filter (query bf . exportRecord) $ withMask r (permute mask)

type DataSet = V.Vector Record

toBloom :: Int -> Int -> DataSet -> BloomFilter
toBloom bits hashes = V.foldl (flip (.) exportRecord . flip add) $ newBloomFilter bits hashes
