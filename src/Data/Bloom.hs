{-# LANGUAGE FlexibleInstances #-}

-- | This module contains utility functions for working with Bloom filters, which are a probabilistic way to check for set membership.  The guarantee no false negatives and a dined false positive rate.
module Data.Bloom (newBloomFilter, estimate, contains, BloomFilter, query, add, bloomFilterFromFile, falsePositiveRate, bitCount, combine) where

import Control.Monad.ST (runST)
import Control.Monad.Primitive
import Data.Binary
import Data.Bits
import Data.ByteString (unpack)
import Data.ByteString.Lazy (toStrict, take, ByteString)
import Data.Traversable (forM)
import qualified Data.Vector.Storable as V --(unsafeFromForeignPtr)
import qualified Data.Vector.Storable.Mutable as VM
import Data.Digest.Pure.SHA (sha512, bytestringDigest)
import Foreign.ForeignPtr
import Foreign.Ptr
import Foreign.Storable (peek)
import Prelude
import System.IO.MMap
import System.IO.Unsafe

-- |A set of bits that can be queried for set membership.  The mask is the proper bit field, hashes represents the number of hash functions to be used, and bits is the exact number of bits to use.  The number of bits in the mask is the value of bits founded up to the next multiple of eight.
data BloomFilter = BloomFilter {
  mask :: V.Vector Word8,
  hashes :: Int,
  bits :: Int}
                 deriving Show

data MBloomFilter m = MBloomFilter {
  mmask :: VM.MVector m Word8,
  mhashes :: Int,
  mbits :: Int}

instance Binary BloomFilter where
  put x = do
    put $ bits x
    put $ hashes x
    V.forM_ (mask x) put
  get = do
    bits' <- get
    hashes' <- get
    mask' <- getVector (bitsToBytes bits')
    return BloomFilter {
      mask = mask',
      hashes = hashes',
      bits = bits'
      }

-- | Retruns the number of bits in a Bloom filter that have been set to true.
bitCount :: BloomFilter -> Int
bitCount = V.sum . V.map popCount . mask

-- | Calculates the probability that a random query will return true for a given Bloom filter.
falsePositiveRate :: BloomFilter -> Double
falsePositiveRate bf = p^^hashes bf
  where
    p = fromIntegral (V.sum $ V.map popCount $ mask bf) / fromIntegral (bits bf)

-- | Reads a bloom filter at a given file path.  Note that, to save on memory overhead for very large bloom filters, this function uses a memory mapped file to avoid reading the entire data set.  As a consequence, if the file is altered by an outside program, the value of the bloom filter will change in the program.  This is obviously the exact opposite of immutability, but it's a trade-off to be able to handle bloom filters large that physical memory.
bloomFilterFromFile :: FilePath -> IO BloomFilter
bloomFilterFromFile file = mmapFileForeignPtr file ReadOnly Nothing >>= bloomFilterFromFile'

bloomFilterFromFile' :: (ForeignPtr Word8, Int, Int) -> IO BloomFilter
bloomFilterFromFile' (wordPtr, _, _) = do
  bits' <- withForeignPtr wordPtr networkInt
  hashes' <- withForeignPtr wordPtr (networkInt .( `plusPtr` 8))
  return BloomFilter {
    bits = bits',
    hashes = hashes',
    mask = V.unsafeFromForeignPtr wordPtr 16 $ bitsToBytes bits'}

networkInt :: Ptr Word8 -> IO Int
networkInt ptr = do
  bs <- forM [0..7] $ \x -> peek ((castPtr ptr :: Ptr Word8) `plusPtr` x) :: IO Word8
  return $ foldl (\a b -> 256*a+fromIntegral b) 0 bs


getVector :: (Binary a, VM.Storable a) => Int -> Get (V.Vector a)
getVector n = do
  v <- return . unsafePerformIO . VM.unsafeNew $ n
  let go 0 = return ()
      go i = do
        x <- get
        () <- return $ unsafePerformIO $ VM.unsafeWrite v (n-i) x
        go (i-1)
  () <- go n
  return . unsafePerformIO . V.unsafeFreeze $ v


bitsToBytes :: Int -> Int
bitsToBytes x =
  if x `mod` 8 == 0
  then n
  else n+1
  where
    n = x `div` 8

-- |Creates an empty Bloom Filter of the given size in bits using the given number of hashes
newBloomFilter :: Int -> Int -> BloomFilter
newBloomFilter bits' hashes' = BloomFilter{ bits= bits',
                                            hashes = hashes',
                                            mask = V.replicate count 0
                                          }
  where
    count = bitsToBytes bits'

-- |Gives a very crude estimate for the number of entries in the filter.  Has the best accuracy for largely empty filters and the worst for largely full ones.
estimate :: BloomFilter -> Int
estimate x = (`div` hashes x) . V.sum . V.map popCount . mask $ x

-- |Test whether one Bloom filter contains another
contains :: BloomFilter -> BloomFilter -> Bool
contains a = V.all (== 0) . V.zipWith (.&.) (mask a) . V.map complement . mask

-- |Test whether a given string is in a Bloom Filter
query :: BloomFilter -> ByteString -> Bool
query bf = checkBits (mask bf) . digestToNumbers (fromIntegral $ bits bf) (hashes bf) . stringToMask

checkBits :: V.Vector Word8 -> V.Vector Int -> Bool
checkBits mask' = V.all (checkBit mask')

checkBit :: V.Vector Word8 -> Int -> Bool
checkBit mask' n =
  let outer = fromIntegral $ n `div` finiteBitSize (0 :: Word8)
      inner = fromIntegral $ n `mod` finiteBitSize (0 :: Word8)
  in
    (mask' V.! outer) `testBit` inner

setBits :: (PrimMonad m) => VM.MVector (PrimState m)Word8 -> V.Vector Int -> m ()
setBits mask' bits' = V.forM_ bits' $ updateBits mask'

updateBits :: (PrimMonad m) => VM.MVector (PrimState m) Word8 -> Int -> m ()
updateBits v n =
  let outer = fromIntegral $ n `div` finiteBitSize (0 :: Word8)
      inner = fromIntegral $ n `mod` finiteBitSize (0 :: Word8)
  in VM.modify v (`setBit` inner) outer

digestToNumbers :: Word64 -> Int -> (Word64, Word64) -> V.Vector Int
digestToNumbers bits' hashes' (a, b) = V.map f $ V.generate hashes' fromIntegral
  where
    f :: Word64 -> Int
    f x = fromIntegral $ (a+x*b) `mod` bits'

updateMask :: PrimMonad m => MBloomFilter (PrimState m) -> (Word64, Word64) -> m ()
updateMask bf m = setBits (mmask bf) $ digestToNumbers (fromIntegral $ mbits bf) (mhashes bf) m

-- | Insert a string into a Bloom filter
add :: ByteString -> BloomFilter -> BloomFilter
add x bf = runST $ do
  mbf <- thawBloomFilter bf
  mbf2 <- insert x mbf
  freezeMBloomFilter mbf2

insert :: PrimMonad m => ByteString -> MBloomFilter (PrimState m) -> m (MBloomFilter (PrimState m))
insert value bf = do
  updateMask bf $ stringToMask $ value
  return bf

stringToMask ::ByteString -> (Word64, Word64)
stringToMask = pack64 . byteStringToWords . sha

sha :: ByteString -> ByteString
sha = bytestringDigest . sha512

byteStringToWords :: ByteString -> [Word8]
byteStringToWords = unpack . toStrict . Data.ByteString.Lazy.take 16

pack64 :: [Word8] -> (Word64, Word64)
pack64 xs = (
  foldl (\a b -> 256 * a + fromIntegral b) 0 (Prelude.take 8 xs),
  foldl (\a b -> 256 * a + fromIntegral b) 0 (Prelude.take 8 $ drop 8 xs))

thawBloomFilter :: (PrimMonad m) => BloomFilter -> m (MBloomFilter (PrimState m))
thawBloomFilter x = do
  mmask' <- V.thaw $ mask x
  return MBloomFilter {
    mmask = mmask',
    mbits = bits x,
    mhashes = hashes x
    }

freezeMBloomFilter :: (PrimMonad m) => MBloomFilter (PrimState m) -> m BloomFilter
freezeMBloomFilter x = do
  mask' <- V.freeze $ mmask x
  return BloomFilter {
    mask = mask',
    bits = mbits x,
    hashes = mhashes x
    }

combine :: BloomFilter -> BloomFilter -> Either String BloomFilter
combine a b = do
  let failWhen x True = Left x
      failWhen _ False = Right ()
  failWhen "Different number of bits" $ bits a /= bits b
  failWhen "Different number of hashes" $ hashes a /= hashes b
  return BloomFilter {bits = bits a, hashes = hashes a,
                      mask = V.zipWith (.|.) (mask a) (mask b)}
