module Loader where

import Control.Monad ((>=>))
import Control.Monad.IO.Class (liftIO, MonadIO)
import Control.Monad.Trans.Except
import Data.Aeson (eitherDecode)
import Data.Bloom
import Data.Bloom.MetaData
import Data.ByteString.Lazy as BSL
import Data.Csv (decode,HasHeader(..))
import Data.Text (toLower)
import Database.HDBC (ConnWrapper(..),quickQuery',fromSql)
import Database.HDBC.ODBC (connectODBC)
import Database.HDBC.Sqlite3 (connectSqlite3)
import qualified Data.Vector as V
import Options.Applicative

-- |Turn a CSV file into a vector of vector strings
getCells :: MonadIO m => FilePath -> ExceptT String m DataSet
getCells = (liftIO . BSL.readFile) >=> ExceptT . return . decode NoHeader

isolateRelevant :: V.Vector Bool -> DataSet -> DataSet
isolateRelevant mask = V.map isolate'
  where
    isolate' = V.map snd . V.filter fst . V.zip mask

data SqlConn = Sqlite FilePath
             | ODBC String

parserSqlite :: Parser SqlConn
parserSqlite = Sqlite <$> strArgument (metavar "INFILE"
                                        <> help "Sqlite database file")
parserODBC :: Parser SqlConn
parserODBC = ODBC <$> strArgument (metavar "CONNETION"
                                   <> help "ODBC Connection String")

sqlOptions :: Parser SqlConn
sqlOptions = subparser $
  command "sqlite"
  (parserSqlite `withInfo` "Access Sqlite database file")
  <> command "odbc"
  (parserODBC `withInfo` "Access any ODBC accessible database")

sqlConn :: MonadIO m => SqlConn -> m ConnWrapper
sqlConn (Sqlite f) = liftIO $ connectSqlite3 f >>= (return . ConnWrapper)
sqlConn (ODBC s) = liftIO $ connectODBC s >>= (return . ConnWrapper)

data ImportOptions = CsvFile FilePath [Bool]
                   | Sql SqlConn String

withInfo :: Parser a -> String -> ParserInfo a
withInfo opts desc = info (helper <*> opts) $ progDesc desc

parseSql :: Parser ImportOptions
parseSql = Sql <$> sqlOptions
           <*> strArgument (metavar "QUERY"
                         <> help "The SQL query to be performed")
parseCsvFile :: Parser ImportOptions
parseCsvFile = CsvFile
  <$> strArgument (metavar "INFILE"
                    <> help "The CSV file to read")
  <*> argument auto (metavar "COLUMNS"
                    <> help "A list of which columns to accept")

readOptions :: Parser ImportOptions
readOptions = subparser $
  command "csv" (parseCsvFile `withInfo`"Read records from a CSV file")
  <> command "sql" (parseSql `withInfo` "Pull data from an SQL database.")

loadData :: (MonadIO m) => ImportOptions -> ExceptT String m DataSet
loadData (CsvFile f mask) = do
  result <- getCells f
  return $ isolateRelevant (V.fromList mask) $ V.map (V.map (toLower)) result
loadData (Sql conn queryString) = do
  c <- sqlConn conn
  result <- liftIO $ quickQuery' c queryString []
  return . V.map (V.map (toLower . fromSql) . V.fromList) . V.fromList $ result


data BloomLoader = Simple FilePath |
                   UniquePath FilePath FilePath

parseSimple :: Parser BloomLoader
parseSimple = Simple <$> strArgument (metavar "BLOOMFILE")

parseUniquePath :: Parser BloomLoader
parseUniquePath = UniquePath
                  <$> strArgument (metavar "BLOOMFILE")
                  <*> strArgument (metavar "METAFILE")

loadBloom :: MonadIO m => BloomLoader -> ExceptT String m (BloomFilter, V.Vector MetaData)
loadBloom (Simple f) = do
  bf <- liftIO $ bloomFilterFromFile f
  meta <- liftIO . BSL.readFile $ f <> ".json"
  ExceptT . return $ (,) <$> pure bf <*> eitherDecode meta
loadBloom (UniquePath b m) = do
  bf <- liftIO $ bloomFilterFromFile b
  meta <- liftIO . BSL.readFile $ m
  ExceptT . return $ (,) <$> pure bf <*> eitherDecode meta

bloomOptions :: Parser BloomLoader
bloomOptions = subparser $
  command "simple"
  (parseSimple `withInfo` "Assume the metadata path from the bloom filter path")
  <> command "unique" (parseUniquePath `withInfo` "Individually load a bloom filter and its metadata")
