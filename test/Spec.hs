{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson (decode, encode)
import Data.Bloom
import Data.Bloom.MetaData
import Data.ByteString.Lazy.UTF8 (fromString)
import Data.Monoid ((<>))
import qualified Data.Vector as V
import Test.Hspec
import Test.Hspec.QuickCheck (prop)
import Test.QuickCheck ((===))
import Test.QuickCheck.Property (Property)
import Test.QuickCheck.Modifiers


bloomSpec :: SpecWith ()
bloomSpec = describe "Bloom Filters" $ do
  let empty = newBloomFilter 1120 5
  prop "hold values" $ \ txt ->
    query (add (fromString txt) empty) $ fromString txt
  prop "rarely lie" $ \ txt txt2->
    not $ txt /= txt2 && query (add (fromString txt) empty) (fromString txt2)
  prop "hold many values" $ \ts ->
    let packed = foldl (flip add) empty $ map fromString ts
    in
        all (query packed . fromString) ts
  file <- runIO $ bloomFilterFromFile "titles.bloom"
  it "read files" $ query file "Anarchism"
  it "lack garbage" $ not $ query file "Ferrarri"
  it "have deep cuts" $ query file "Ferrari"
  prop "holds eveything" $ \(Positive bits) (Positive hashes) dat ->
    let
      bf = toBloom bits hashes $ V.map (V.map fromString . V.fromList) $ V.fromList dat
    in
      V.all (query bf . exportRecord) $ V.map (V.map fromString . V.fromList) $ V.fromList dat

metaJsonTest :: MetaData -> Property
metaJsonTest x = Just x === (decode $ encode x)

metaSpec :: SpecWith ()
metaSpec = describe "Metadata" $ do
  prop "can be converted to JSON" metaJsonTest
  prop "has permutation algebra" $ \x y ->
    permutations (V.fromList x) * permutations (V.fromList y) === permutations (V.fromList x <> V.fromList y)
  it "can permute different options" $
    permute (a <> b <> c) === combined
  where
    a = V.singleton $ Entry "Name"
    b = V.singleton $ Option "Gender" $ V.fromList ["Male", "Female"]
    c = V.singleton $ Range "Year" 1928 1947
    combined = do
      x <- permute a
      y <- permute b
      z <- permute c
      return $ x <> y <> z

mySpec :: SpecWith ()
mySpec = describe "everything" $ do
  bloomSpec
  metaSpec

main :: IO ()
main = hspec mySpec
